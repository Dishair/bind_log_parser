#!/usr/bin/python3

import time
import re
from datetime import datetime
import psycopg2
from psycopg2 import Error


conn = psycopg2.connect(user="postgres",
                                  password="password",
                                  host="127.0.0.1",
                                  port="5432",
                                  database="radius")

cur = conn.cursor()

# Задаем последнюю строку для начала чтения
lastline = open('/var/lib/bind/lastline.txt')
try:
    num = (int(lastline.read()))
except:
    with open('/var/lib/bind/lastline.txt', 'w') as lastline:
        lastline.write(str(0))
        num = (int(lastline.read())) 
        
print (num, "num до проверки")


# Находим строку с записью о DNS-запросах клиентов 
with open('/var/lib/bind/query.log') as logfile:
    # Считаем количество строчек
    all_lines = len (logfile.readlines())
    print (all_lines, "линий в файле")
    # Если произошла ротация логов - читаем лог с начала
    # print (num)
    if num > (all_lines - 1):
        with open('/var/lib/bind/lastline.txt', 'w') as lastline:
            lastline.write(str(0))

# Задаем последнюю строку для начала чтения
lastline = open('/var/lib/bind/lastline.txt')
num = (int(lastline.read()))
print (num, "num после проверки")


# Находим строку с записью о DNS-запросах клиентов 
with open('/var/lib/bind/query.log') as logfile:
    lines = logfile.readlines()[(num):]        
    # print (lines)
    for line in lines:
        try:
            # print (line)
            # Находим точный IP
            match = re.search(r'10.1.\d{1,3}.\d{1,3}', line)
            if match:
                #print ((match.group()))
                matchip = ((match.group()))
            # Находим точный URI
            match = re.search(r'\(\S*\)', line)
            if match:
                #print (str((match.group()))[1:-1])
                matchuri = (str((match.group()))[1:-1])
            # Находим точную дату
            match = re.search(r'.{20}', line)
            if match:
                # Превращаем строку в время
                #print (datetime.strptime(str(match.group()), "%d-%b-%Y %H:%M:%S"))
                matchtime = (datetime.strptime(str(match.group()), "%d-%b-%Y %H:%M:%S"))
            # Находим последнюю по времени строку, где есть нужный нам IP
            # print (matchip)
            cur.execute('SELECT radacctid, callingstationid FROM radacct WHERE framedipaddress = %s', (matchip, ))
            listofmac = []
            for row in cur:
                listofmac.append(row)
                if (row[0]) > ((listofmac[0])[0]):
                    matchmac = (row[1])

            # Заносим данные в базу      
            cur.execute("INSERT INTO history (clientipaddress, clienturi, clienttime, clientmacaddress) \
            VALUES (%s, %s, %s, %s)", (matchip, matchuri, matchtime, matchmac))
            conn.commit()
        except Exception:
            pass

# Записываем последнюю строку в файл
with open('/var/lib/bind/lastline.txt', 'w') as lastline:
    lastline.write(str(all_lines - 1))
                
print (all_lines - 1, "num после обработки")

#print (listofmac)
#print (matchmac)